# azure_cfe

## create vm
F5 byol
- create mgmt interface first
- add additional nics

AZ, if you use an AZ you need to make sure the public ip's are in the same zone (this may come back to haunt me and assign to Region) 
- note this worked with AZ's and public IP's in AZ
- set app public ip to regional  

## CFE Documentation

Link to F5 [CFE docs](https://clouddocs.f5.com/products/extensions/f5-cloud-failover/latest/userguide/azure.html#azure-msi)

CFE rpm [download](https://github.com/F5Networks/f5-cloud-failover-extension)
## Restjavad

Increase memory size as indicated [here](https://clouddocs.f5.com/products/extensions/f5-cloud-failover/latest/userguide/quickstart.html)

Provision management to Large

```
tmsh modify sys db provision.extramb value 1000
tmsh modify sys db restjavad.useextramb value true
```
Restart restjavad daemons:
```
bigstart restart restjavad restnoded
```

# CFE specifics
As the docs state:
- It looks at all the Ipconfigs on the NICs
- It looks at all the VIPs on the F5 config. It only looks at VIPs in traffic-group-1 (ie only floating IPs, it won’t move a self IP and doesn’t support other traffic groups).
- If there is a ipconfig in Azure that is listed as a VIP in F5 config, then the IP will be considered part of the ipconfigs to move.

Why does this exist? [K06002086](https://support.f5.com/csp/article/K06002086)


#  Declaration

GET config endpoint:
```
curl --location --request GET 'https://192.0.2.1:443/mgmt/shared/cloud-failover/declare' --header 'Authorization: Basic YXdzdXNlcjpwYXNzd29yZC0xMjM='
```

POST declaration:
```
curl --location --request POST 'https://192.0.2.1:443/mgmt/shared/cloud-failover/declare' --header 'Content-Type: application/json'
```
[Postman](https://clouddocs.f5.com/products/extensions/f5-cloud-failover/latest/userguide/postman-collection.html)


-    declare: user this endpoint to configure CFE
-    declare: user this endpoint to configure CFE.
-    info: use this endpoint to get information on CFE, such as the version number.
-    inspect: use this endpoint to list associated cloud objects.
-    reset: use this endpoint to reset the failover state file.
-    trigger: use this endpoint to trigger failover.

Example declaration here [sample_cfe_dec.json](sample_cfe_dec.json)

# Troubleshooting / Validation

On any initial configuration or re-configuration, F5 recommends that you validate Cloud Failover Extension’s configuration to confirm it can properly communicate with the cloud environment and what actions will be performed.

On the Standby instance:

Inspect the configuration to confirm all the BIG-IPs interfaces have been identified.

Use the */inspect* endpoint to list associated cloud objects.

For example:
```
curl -su admin: http://localhost:8100/mgmt/shared/cloud-failover/inspect | jq .
```
Peform a Dry-Run of the Failover to confirm what addresses or routes have been identified and will be remapped.

Use the */trigger* endpoint with '{"action":"dry-run"}' payload.

For example:
```
curl -su admin: -X POST -d '{"action":"dry-run"}' http://localhost:8100/mgmt/shared/cloud-failover/trigger | jq 
```

# View restnoded logs

[Logging](https://clouddocs.f5.com/products/extensions/f5-cloud-failover/latest/userguide/logging.html)
```
tail -f /var/log/restnoded/restnoded.log
```

## Additional ways to check your CFE config
Reference your storage account, this is where CFE stores the state failover

![](img/azureCFEsa.PNG)

You will see your ``f5cloudfailoverstate.json`` file

![](img/azureCFEfile.PNG)

An example file is provided [f5cloudfailoverstate.json](f5cloudfailoverstate.json)

### QKVIEW

View deployed cfe config under `config/cloud/azure/`
